#!/usr/bin/python2
import datetime
from suds.client import Client
from suds.sax.element import Element
import hashlib
import logging
import datetime
import re
import ast
import urllib
import urllib2
import redis
from bs4 import BeautifulSoup, NavigableString
import logging
import logging.config
import MySQLdb
import sdp_mysql as MySQL
from sdp_configs import sdp_params, logger

class SDP_Queue_MySQL():               
        
    def execute_query(self, db_connection, sql, params):
        """ Insert data into the database
        Keyword arguments:
            db_connection -- The connection to the database.
            sql -- The sql to be executed.
            params -- A parameter list holding values to be used in the sql.        
        """
        try:
            cursor = db_connection.cursor()
            cursor.execute(sql, params)
        except MySQLdb.DatabaseError:
            raise
        except TypeError:
            raise

    def retrieve_all_data_params(self, db_connection, sql, params):
        """ Retrieve Data from the database
        
            Retrieve data from the database, returns a dictionary that 
            holds the results.
        
            Keyword arguments:
            db_connection -- The connection to the database.
            sql -- The sql to be executed.
            params -- A parameter list holding values to be used in the sql.
        
        """
        results = None
        try:
            cursor = db_connection.cursor()
            cursor.execute(sql, params)
            results = cursor.fetchall()
        except MySQLdb.DatabaseError:
            raise
        return results
    
    def retrieve_all_data(self, db_connection, sql):
        """ Retrieve Data from the database
        
            Retrieve data from the database, returns a dictionary that 
            holds the results.
        
            Keyword arguments:
            db_connection -- The connection to the database.
            sql -- The sql to be executed.
        
        """
        results = None
        try:
            cursor = db_connection.cursor()
            cursor.execute(sql)
            results = cursor.fetchall()
        except MySQLdb.DatabaseError:
            raise
        return results

class SDP():

    def __init__(self):        
        self.count = 1
        self.headers = {'Content-Type': 'text/xml; charset=utf-8'}
        self.client = Client(sdp_params['sdp_wsdl'], headers=self.headers) 
        self.client.options.location = sdp_params['send_location']
        logging.basicConfig(level=logging.INFO)
        self.logger = logging.getLogger('suds.client')
        self.logger.setLevel(logging.DEBUG)
        
    def set_soap_values(self, originator, destination, link_id, service_id):
        # Define Charging information element
        ci = self.client.factory.create('ns0:ChargingInformation')
        ci.amount = sdp_params['charge_amount']
        #ci.code = '691'
        ci.currency = sdp_params['charge_currency']
        ci.description = sdp_params['charge_description']
        
        # Define Receipt request
        self.rr = self.client.factory.create('ns0:SimpleReference')
        self.rr.endpoint = sdp_params['endpoint']
        
        self.rr.interfaceName = 'SmsNotification'

        #Generate a correlator
        self.access_no = destination
        logger.info("access_no" + self.access_no)
        self.correlator = str(self.access_no) + str(self.count).zfill (20)
        if str(self.correlator) == (str(self.access_no) + str(9)*20):
            self.count = 1
            self.correlator = str(self.access_no) + str(self.count).zfill (20)
        self.rr.correlator = self.correlator
        self.count += 1 #Increment the count
        logger.info("Correlator: " + self.correlator)

        # Hash password
        timestamp = datetime.datetime.strftime(datetime.datetime.now(), '%Y%m%d%H%M%S')
        sp_id = sdp_params['sp_id']
        service_id = service_id
        password =  sdp_params['password']
        m = hashlib.md5()
        m.update(sp_id + password + timestamp)
        sp_password = m.hexdigest()
        self.headerns = ('v2', 'http://www.huawei.com.cn/schema/common/v2_1')
        spIdElement = Element('spId', ns=self.headerns).setText(sp_id)
        spPasswordElement = Element('spPassword', ns=self.headerns).setText(sp_password)
        serviceIdElement = Element('serviceId', ns=self.headerns).setText(service_id)
        timestampElement = Element('timeStamp', ns=self.headerns).setText(timestamp)
        linkId = Element('linkid', ns=self.headerns).setText(link_id)
        oaElement = Element('OA', ns=self.headerns).setText(originator)
        faElement = Element('FA', ns=self.headerns).setText(originator)

        # Create the SOAP header
        requestSOAPHeader = Element('RequestSOAPHeader', ns=self.headerns)
        requestSOAPHeader.insert(faElement)
        requestSOAPHeader.insert(oaElement)
        requestSOAPHeader.insert(linkId)
        requestSOAPHeader.insert(timestampElement)
        requestSOAPHeader.insert(serviceIdElement)
        requestSOAPHeader.insert(spPasswordElement)
        requestSOAPHeader.insert(spIdElement)

        self.client.set_options(soapheaders=requestSOAPHeader)

    def send_sms(self, msg, originator, destination, link_id, service_id):
        """ Sends a message and returns the a dictionary containing:
                 correlator and the result (requestIdentifier) used to send the message 
                else returns None
        """
        logger.info(msg)
        if msg == None:
            raise Exception("Empty Message", "Cannot send an empty message.")
        
        #set soap_header
        self.set_soap_values(originator, destination, link_id, service_id)   
        result = None
        try:
            result = self.client.service.sendSms(originator, destination, None, msg, self.rr)
        except Exception, e:
            raise Exception("SendSMS", "An error occured while sending sms.") 

        response = {'correlator':self.correlator, 'request_id':result}        
        if result:
            logger.info("Got a result from sendSMS-SDP: " + result)
        else:
            logger.error("No result from sendSMS-SDP!")
        return response
