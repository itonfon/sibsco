#!/usr/bin/python

from sdp import SDP_Queue_MySQL, SDP
import time
import datetime
import logging
import ConfigParser
from sdp_configs import logger, config, mysql_params 
import os
import signal
import MySQLdb
import sys         
import ConfigParser

is_sending = 0
is_waiting_to_die = 0
sdp = SDP()
sdp_queue_mysql = SDP_Queue_MySQL()

def signal_handler(signum, frame):
    global is_sending, is_waiting_to_die
    if is_sending:
        is_waiting_to_die = 1
        logger.info('Process sending: Wait to die gracefully! ')
        return 
    logger.info('Process not sending: Die ')
    exit(0) 

def service_params(service):
    sections = config.sections()
    try:
        index = sections.index(service)
        name = config.get(sections[index],'name')
        service_id = config.get(sections[index],'service_id')
        access_no = config.get(sections[index],'access_no')
        correlator = config.get(sections[index],'correlator')
        params = {'name':name, 'service_id':service_id, 
                'access_no':access_no, 'correlator':correlator}
        return params
    except Exception, e:
        raise


def service_init(args):
    """ Defines how the application is started.
    args = terminal arguments used to run the service """
    
    app_list = config.sections()
    reserved_sections = ['sdp', 'mysql', 'logger', 'sdp_start', 'sdp_stop']

    #Invalid Arguments or Invalid Params
    if len(args) < 2:
        logger.error("USAGE: python %s <app_name>" % args[0])         
        exit(0)
    elif sys.argv[1] not in app_list:
        logger.error('Invalid Args: App %s must be in the config file' % args[1])
        exit(0)
    elif args[1] in reserved_sections:
        logger.error('Invalid Args: %s is a reserved section' % args[1])
        exit(0)
    else:
        try:
            params = service_params(args[1])
            logger.info('%s: %s Started! Name: %s, Correlator : %s, Access No : %s' 
                    % (datetime.datetime.now(), args[1], params['name'], \
                            params['correlator'], params['access_no']))
            return params
        except Exception, e:
            logger.error('Invalid Params in the configuration file. %s' % e)
            exit(0)
  
def _fetch_from_queue(access_no=None, connection=None):
    """ Retrieve from messages from the database    
    Params:
        direction --- IN for message direction IN and OUT for OUT   
    """
    
    sql = """ SELECT RecordID, Originator, Destination, Message, MessageTimeStamp, 
                  MessageDirection, SMSCID
              FROM dbQueue WHERE MessageDirection = 'OUT' AND 
              Originator = %s LIMIT 1000"""
    params = (access_no, )
    try:
        msg_list = sdp_queue_mysql.retrieve_all_data_params(connection, sql, params)
        return msg_list
    except Exception, e:
        logger.error(e)
        raise
 
def _delete_msg(record_id, connection):
    logger.info('Deleting msg with ID : %s ' % record_id)
    del_sql = 'DELETE FROM dbQueue WHERE RecordID = %s '    
    params = (int(record_id),)
    try:
        sdp_queue_mysql.execute_query(connection, del_sql, params)
        logger.info('Deleted!')
        return True
    except Exception, e:
        logger.error('_delete_msg: %s' % e)
        raise    
          
def _pop_link_id(access_no, originator, connection):
    """ Pop a msg with link_id. Returns a dictionary
    Params:
        access_no ---  The service short code
        originator --- Origin of the message
    """
    #Select
    logger.info('AccessNo =  %s and Originator = %s' % (access_no, originator))
    select_sql = """SELECT RecordID, Originator, LinkId, AccessNo  
                    FROM dbLinkId WHERE AccessNo = %s AND Originator = %s Limit 1"""       
    select_params = (access_no, originator) 
    try:
        logger.info('Retrieve all_data_params')
        msg_row = sdp_queue_mysql.retrieve_all_data_params(connection, select_sql, select_params)
        logger.info( msg_row[0])        
        del_sql = "DELETE FROM dbLinkId WHERE RecordID = %s "
        del_params = (int(msg_row[0]['RecordID']),)     
        sdp_queue_mysql.execute_query(connection, del_sql, del_params)
        return msg_row[0]
    except Exception, e:
        logger.error('Pop Link ID Exception:')
        logger.error(e)
        raise

def _push_to_cdr(msg, correlator, request_id, connection):
    insert_sql = """INSERT INTO dbCDR(Originator, Destination, Message, 
                       MessageTimeStamp, MessageID, SMSCID, issent, Correlator,
                       RequestIdentifier) 
		    VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s)"""
    timestamp = datetime.datetime.strftime(datetime.datetime.now(), '%Y%m%d%H%M%S')

    params = (msg['Originator'], msg['Destination'], msg['Message'], 
		    timestamp,'0', '0', 1, correlator,
                    request_id)
    try:
        sdp_queue_mysql.execute_query(connection, insert_sql, params)
        return True
    except Exception, e:
        logger.error(e)
        raise

def _push_to_dlrqueue(request_id, access_no, msisdn, connection):
    insert_sql = """ INSERT INTO dbDeliveryReceiptsQueue 
                        (RequestIdentifier, AccessNo, MSISDN, MessageTimeStamp)
                    VALUES(%s, %s, %s, %s)"""
    timestamp = datetime.datetime.strftime(datetime.datetime.now(), '%Y%m%d%H%M%S')
    params = (request_id, access_no, msisdn, timestamp)
    try:
        sdp_queue_mysql.execute_query(connection, insert_sql, params)
        return True
    except Exception, e:
        logger.error(e)
        raise

def dispatch(service_params, connection):
    """ Pops sms from queue, sends it destination, record transaction to cdr 
    """
    global is_sending, is_waiting_to_die
    access_no = service_params['access_no'] 
    service_id = service_params['service_id']

    #Fetch SMS where direction = OUT
    msg_list = _fetch_from_queue(access_no=access_no, connection=connection)

    #Loop through SMS
    for msg in msg_list:
        try:
            #Delete from queue
            logger.info("Delete MSG from dbQueue")
            _delete_msg(msg['RecordID'], connection)
            
            #Pop link_id
            logger.info("Pop linkid ...")
            db_linkid_msg = _pop_link_id(access_no=msg['Originator'], \
                                         originator=msg['Destination'],\
                                         connection=connection)
            
            #Send only when there's a linkid
            if not db_linkid_msg:
                raise Exception("Linkid:", "No corresponding linkid")
            
            is_sending = 1
            logger.info('Sending = True')
            destination = "tel:" + msg['Destination']
            logger.info('Linkid: %s, Destination: %s, Message: %s ' % \
                    (db_linkid_msg['LinkId'], destination, msg['Message']))
            response = sdp.send_sms(msg['Message'].decode('utf8', 'replace'), destination, msg['Originator'],
                    db_linkid_msg['LinkId'], service_id=service_id)
            
            correlator = response['correlator']
            request_id = response['request_id']
            logger.info("Got a correlator: %s and RequestIdentifier: %s " 
                    % (correlator, request_id))
            
            #Raise an exception if no requestIdentifier.
            if not request_id:
                raise  Exception("Send SMS Failed:", "No request_id from SDP")
            
            #Record to cdr
            _push_to_cdr(msg, correlator, request_id, connection)

            #Record to dlr queue for offline polling
            #_push_to_dlrqueue(request_id, access_no, msg['Destination'] , connection)

            #Commit the connection
            connection.commit()

            #Handle kill signal
            is_sending = 0
            logger.info('Sending = False ')
            if is_waiting_to_die:
                logger.info('Finished sending. Now Dying ... ')
                exit(0)

        except Exception, e:
            logger.error(e)
            connection.rollback()
            logger.info("Sending in Exception = False")
            is_sending = 0 #It's alright to die

if __name__=='__main__':
    service_params = service_init(sys.argv)
    signal.signal(signal.SIGTERM, signal_handler)

    while True:
        try:
            connection = MySQLdb.connect(host=mysql_params['host'], 
                            user=mysql_params['user'], passwd=mysql_params['passwd'], 
			    db=mysql_params['db'], cursorclass=MySQLdb.cursors.DictCursor)
            dispatch(service_params, connection)
        except MySQLdb.Error, e:
            logger.error("MySQL Error: %s" % e)
        finally:
            try:
                connection.close()
            except Exception ,e:
                logger.error("MySQL Error closing connection : %s" % e)
              
        time.sleep(1)
