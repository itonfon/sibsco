import time
import logging
import ConfigParser
from sdp_mysql import MySQL

CONFIG_FILE = r'/usr/local/lib/send_sms_mo/sdp.conf'
config = ConfigParser.ConfigParser()
config.read(CONFIG_FILE)

#Get the global details
access_file = config.get("sdp", "access_file")
access_config = ConfigParser.ConfigParser()
access_config.read(access_file)

#logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("SDP")
logger.setLevel(logging.DEBUG)
hdlr = logging.FileHandler(config.get("logger", "log_file"))
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s ')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)

#logging.basicConfig(level=logging.INFO)
sn_logger = logging.getLogger("SDP_STARTNOTIFICAITON")
sn_logger.setLevel(logging.DEBUG)
hdlr = logging.FileHandler(config.get("logger", "sdp_startnotification_log_file"))
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)

sn_logger.addHandler(hdlr)
#logging.basicConfig(level=logging.INFO)
st_logger = logging.getLogger("SDP_STOPNOTIFICAITON")
st_logger.setLevel(logging.DEBUG)
hdlr = logging.FileHandler(config.get("logger", "sdp_stopnotification_log_file"))
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
st_logger.addHandler(hdlr)

host = config.get("mysql", "host")
port = config.get("mysql", "port")
user = config.get("mysql", "user")
passwd = config.get("mysql", "password")
db = config.get("mysql", "database")
connection_timeout = config.get("mysql", "connection_timeout")
mysql_params = {
        'host':host,
        'port':port,
        'user':user,
        'passwd':passwd,
        'db':db,
        'connection_timeout':connection_timeout
        }

sdp_wsdl = config.get("sdp", "sdp_wsdl")
endpoint = config.get("sdp", "endpoint")
send_location = config.get("sdp", "send_location")
sp_id = access_config.get("sdp", "sp_id")
password = access_config.get("sdp", "password")

charge_amount = config.get("sdp", "charge_amount")
charge_code = config.get("sdp", "charge_code")
charge_currency = config.get("sdp", "charge_currency")
charge_description = config.get("sdp", "charge_description")
sn_wsdl = config.get('sdp_start', 'wsdl')
sdp_wsdl_mo_poll = config.get('sdp', 'sdp_wsdl_mo_poll')
sn_endpoint = config.get('sdp_start', 'endpoint')
sn_send_location = config.get('sdp_start', 'send_location')
st_wsdl = config.get('sdp_stop', 'wsdl')
st_endpoint = config.get('sdp_stop', 'endpoint')
st_send_location = config.get('sdp_stop', 'send_location')
dlr_send_location = config.get('sdp', 'send_location_dlr')
send_location_mo_poll = config.get('sdp', 'send_location_mo_poll')
sdp_params = {
    'sdp_wsdl':sdp_wsdl,
    'sdp_wsdl_mo_poll':sdp_wsdl_mo_poll,
    'endpoint':endpoint,
    'send_location':send_location,
    'sp_id':sp_id,
    'password':password,
#    'bulk_sp_id': bulk_sp_id,
#    'bulk_password':bulk_password,
    'charge_amount' :charge_amount,
    'charge_code':charge_code,
    'charge_currency':charge_currency,
    'charge_description':charge_description,
    'sn_wsdl':sn_wsdl,
    'sn_endpoint':sn_endpoint,
    'sn_send_location':sn_send_location,
    'st_wsdl':st_wsdl,
    'st_endpoint':st_endpoint,
    'st_send_location':st_send_location,
    'send_location_dlr':dlr_send_location,
    'send_location_mo_poll':send_location_mo_poll
}

