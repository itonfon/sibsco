""" Handles database connection details """
import MySQLdb
import MySQLdb.cursors

class MySQL:
    """Lazy DB proxy."""
    __shared_state = {}
    def __init__(self, host, port, user, passwd, database, connection_timeout):
        self.__dict__ = self.__shared_state
        self.__connection = None
        self.__host = host
        self.__port = port
        self.__user = user
        self.__passwd = passwd
        self.__database = database
        self.__connection_timeout = connection_timeout

    def __getattr__(self, name): 
        if self.__connection is None:
            try:
                self.__connection = MySQLdb.connect(
                    host=self.__host,
                    port=self.__port,
                    user=self.__user,
                    passwd=self.__passwd,
                    db=self.__database,
                    connect_timeout=self.__connection_timeout,
                    cursorclass=MySQLdb.cursors.DictCursor)
            except MySQLdb.Error:
                raise
        return getattr(self.__connection, name)

    def close(self):
        """ Close connection. """
        if self.__connection is not None:
            self.__connection.close()
            self.__connection = None

  
