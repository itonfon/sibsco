#!/usr/bin/python

from MPesa import MPesa
from MySQL import MySQL
from mpesa_configs import logger, config, mysql_params 
from functools import partial

import time
import datetime
import logging
import ConfigParser
import os
import signal
import MySQLdb
import MySQLdb.cursors
import sys
import eventlet

eventlet.monkey_patch()

is_sending = 0
is_waiting_to_die = 0

mysql = MySQL()

#to be accessed in process_mpesa
service_params = None

def signal_handler(signum, frame):
    global is_sending, is_waiting_to_die
    logger.info('SIGNAL:\tSIGTERM')
    if is_sending:
        is_waiting_to_die = 1
        logger.info('SENDING...\t WAIT!')
        return 
    logger.info('DIE')
    exit(0) 

def init_service_params(service):
    """ Parameters specific to the service
    """
    global service_params
    sections = config.sections()
    try:
        index = sections.index(service)
        name = config.get(sections[index],'name')
        initiator = config.get(sections[index],'initiator')
        init_password = config.get(sections[index],'init_password')
        shortcode = config.get(sections[index],'shortcode')
        command_id = config.get(sections[index],'command_id')	
        service_params = {'name':name, 'initiator':initiator, 
                'init_password':init_password, 'shortcode':shortcode,
		'command_id':command_id}
    except Exception, e:
        raise

def service_init(args):   
    """ Defines how the application is started.
    args = terminal arguments used to run the service """
    app_list = config.sections()
    reserved_sections = ['mpesa', 'mysql', 'logger', 'service']

    #Invalid Arguments or Invalid Params
    if len(args) < 2:
        logger.error("USAGE: python %s <app_name>" % args[0])         
        exit(0)
    elif sys.argv[1] not in app_list:
        logger.error('Invalid Args: App %s must be in the config file' % args[1])
        exit(0)
    elif args[1] in reserved_sections:
        logger.error('Invalid Args: %s is a reserved section' % args[1])
        exit(0)
    else:
        try:
            init_service_params(args[1])
            logger.info('%s: %s Started! Name: %s, ShortCode : %s' 
                    % (datetime.datetime.now(), args[1], service_params['name'], \
                            service_params['shortcode']))
        except Exception, e:
            logger.error('Invalid Params in the configuration file. %s' % e)
            exit(0)

def _fetch_from_queue(connection=None):
    """ Retrieve from mpesa_disburse transactions
    """
    
    sql = """SELECT id, transaction_id, ticket_id, msisdn, amount, date_created
    		FROM mpesa_disburse LIMIT 100
	  """
    params = ()
    try:
        trx_list = mysql.retrieve_all_data_params(connection, sql, params)
        return trx_list
    except Exception, e:
        logger.error(e)
        raise

def _delete_transaction(id, connection):
    del_sql = 'DELETE FROM mpesa_disburse WHERE id = %s '    
    params = (int(id),)
    try:
        mysql.execute_query(connection, del_sql, params)
        return True
    except Exception, e:
        logger.error('_delete_transaction: %s' % e)
        raise  

def _push_to_processed(trx, mpesa_transaction_id, mpesa_resp_desc, connection):
    insert_sql = """INSERT INTO mpesa_disburse_complete 
    			(transaction_id, ticket_id, msisdn, amount,  
    			mpesa_transaction_id, mpesa_resp_desc, 
    			date_created, date_processed) 
		    	VALUES(%s, %s, %s, %s, %s, %s, %s, NOW())
		 """
    params = (trx['transaction_id'], trx['ticket_id'], trx['msisdn'], trx['amount'], 
		  mpesa_transaction_id, mpesa_resp_desc, trx['date_created'])
    try:
        mysql.execute_query(connection, insert_sql, params)
        return True
    except Exception, e:
        logger.error(e)
        raise

def dispatch(connection):
    """ Pops transactions from mpesa_disburse 
    """
    global is_sending, is_waiting_to_die
    trx_list = _fetch_from_queue(connection) 
    if not trx_list:
        return
    trxs = []
    trxs.append(trx_list)
    try:
        pool_size = 10
        is_sending = True
        pool = eventlet.GreenPool(size=pool_size)
        for response_list in pool.imap(partial(process_mpesa, connection), trxs):
            for response in response_list:
                logger.info('RESP: OriginatorConversationID:%s, ConversationID:%s, ResponseCode:%s, ServiceStatus:%s, ResponseDesc:%s' %\
                        (response['OriginatorConversationID'], response['ConversationID'], response['ResponseCode'], response['ServiceStatus'],
                         response['ResponseDesc']))
        is_sending = False
        if is_waiting_to_die:
            logger.info('NOT SENDING:\tDIE.')
            exit(0)
    except Exception, e:
        logger.error(e)
        is_sending = 0
        if is_waiting_to_die:
            logger.info('NOT SENDING:\tDIE.')
            exit(0)


def process_mpesa(connection, trx_list):

    if not len(trx_list):
        return [{'OriginatorConversationID':None, 'ConversationID':None}]
    #connection = None
    mpesa = MPesa()
    try:
        #connection = create_connection()
        response_list = []
        for trx in trx_list: 
	    
            try:
                _delete_transaction(trx['id'], connection)

                logger.info('REQ: TRANSACTIONID:%s\MSISDN:%s\AMOUNT:%s'\
                        %(trx['transaction_id'], trx['msisdn'], trx['amount'] ))
                response = mpesa.generic_api_request(service_params, trx['transaction_id'],\
                			 trx['msisdn'], trx['amount'])
                logger.info('RESP: CONVERSATIONID:%s\RESP DESC:%s'\
                        %(response['ConversationID'], response['ResponseDesc'])) 
                conversation_id = response['ConversationID']
                resp_desc = response['ResponseDesc']
                _push_to_processed(trx, conversation_id, resp_desc, connection)
                connection.commit()
                response_list.append(response)
            except Exception, e:
                logger.error(e)
                connection.rollback()
        return response_list
    except Exception, e:
        logger.error(e)

def create_connection():
    """ Creates a connection and returns the connection """
    try:
        connection = MySQLdb.connect(host=mysql_params['host'],\
                user=mysql_params['user'], passwd=mysql_params['passwd'],\
                db=mysql_params['db'], cursorclass=MySQLdb.cursors.DictCursor)
    except MySQLdb.Error, e:
        logger.error(e)
        raise
    return connection

if __name__=='__main__':
    service_init(sys.argv)
    signal.signal(signal.SIGTERM, signal_handler)
    while True:
        try:
            connection = create_connection() 
            dispatch(connection)
        except MySQLdb.Error, e:
            logger.error("MySQL Error: %s" % e)
        finally:
            try:
                connection.close()
            except Exception ,e:
                logger.error("MySQL Error closing connection : %s" % e)
        time.sleep(1)
