import logging                                                                                 
import ConfigParser

CONFIG_FILE = r'/usr/local/lib/mpesa_b2c/mpesa.conf'                            
config = ConfigParser.ConfigParser()                                                           
config.read(CONFIG_FILE)                                                                       
#logging.basicConfig(level=logging.INFO)                                                        
logger = logging.getLogger("MPESA_B2C")                                                              
logger.setLevel(logging.DEBUG)
#logger.setLevel(logging.INFO)
#hdlr = logging.FileHandler(config.get("logger", "log_file"))                                   
hdlr = logging.StreamHandler()                                   
formatter = logging.Formatter('%(asctime)s %(name)s %(levelname)s %(message)s')                         
hdlr.setFormatter(formatter)                                                                   
logger.addHandler(hdlr)                                                                        
                                                                                                                                                           
host = config.get("mysql", "host")                                                             
port = config.get("mysql", "port")                                                             
user = config.get("mysql", "user")                                                             
passwd = config.get("mysql", "password")                                                       
db = config.get("mysql", "database")                                                           
connection_timeout = config.get("mysql", "connection_timeout")                                 
mysql_params = {
        'host':host,
        'port':port,
        'user':user,
        'passwd':passwd,
        'db':db,
        'connection_timeout':connection_timeout        
        }

mpesa_wsdl = config.get("mpesa", "mpesa_wsdl")                                                       
broker_endpoint = config.get("mpesa", "broker_endpoint")                                                       
timeout_endpoint = config.get("mpesa", "timeout_endpoint")                                  
result_endpoint = config.get("mpesa", "result_endpoint")                                                  
public_key = config.get("mpesa", "public_key")   
certificate = config.get("mpesa", "certificate")

sp_id = config.get("service", "sp_id")                                                       
sp_password = config.get("service", "sp_password")                                                       
service_id = config.get("service", "service_id")                                  

broker_params = {                                                                          
    'mpesa_wsdl':mpesa_wsdl,
    'broker_endpoint':broker_endpoint,                                                                
    'timeout_endpoint':timeout_endpoint,                                                      
    'result_endpoint':result_endpoint, 
    'public_key':public_key,
    'certificate':certificate,                                                          
    'sp_id':sp_id,
    'sp_password':sp_password,
    'service_id':service_id,
}                                                                                       
