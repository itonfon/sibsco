#!/usr/bin/env python
from suds.client import Client
from suds.sax.element import Element
from suds.sax.text import Raw
import logging

logging.basicConfig(level=logging.INFO)
logging.getLogger('suds.client').setLevel(logging.DEBUG)

#client = Client('file:///tmp/C2B_SP_kenya.wsdl')
request = Raw("""
<![CDATA[<?xml version="1.0" encoding="UTF-8"?>
<request xmlns="http://api-v1.gen.mm.vodafone.com/mminterface/request">
    <Transaction>
        <CommandID>CommandID0</CommandID>
        <LanguageCode>LanguageCode0</LanguageCode>
        <OriginatorConversationID>OriginatorConversationID0</OriginatorConversationID>
        <ConversationID>ConversationID0</ConversationID>
        <Remark>Remark0</Remark>
        <EncryptedParameters>EncryptedParameters0</EncryptedParameters>
        <Parameters>
            <Parameter>
                <Key>Key0</Key>
                <Value>Value0</Value>
            </Parameter>
            <Parameter>
                <Key>Key1</Key>
                <Value>Value1</Value>
            </Parameter>
        </Parameters>
        <ReferenceData>
            <ReferenceItem>
                <Key>Key2</Key>
                <Value>Value2</Value>
            </ReferenceItem>
        </ReferenceData>
        <Timestamp>
        </Timestamp>
    </Transaction>
    <Identity>
        <Caller>
            <CallerType>0</CallerType>
            <ThirdPartyID>ThirdPartyID0</ThirdPartyID>
            <Password>Password0</Password>
            <CheckSum>CheckSum0</CheckSum>
        </Caller>
        <Initiator>
            <IdentifierType>1</IdentifierType>
            <Identifier>Identifier0</Identifier>
            <SecurityCredential>SecurityCredential0</SecurityCredential>
            <ShortCode>
            </ShortCode>
        </Initiator>
        <PrimartyParty>
            <IdentifierType>1</IdentifierType>
            <Identifier>Identifier1</Identifier>
            <ShortCode>ShortCode0</ShortCode>
        </PrimartyParty>
        <ReceiverParty>
            <IdentifierType>1</IdentifierType>
            <Identifier>Identifier2</Identifier>
            <ShortCode>ShortCode1</ShortCode>
        </ReceiverParty>
        <AccessDevice>
            <IdentifierType>1</IdentifierType>
            <Identifier>Identifier3</Identifier>
        </AccessDevice>
    </Identity>
    <KeyOwner>0</KeyOwner>
</request>]]>
""")
client = Client('file:///home/jason/Private/Projects/lexco/mpesa_b2c/CBPInterface_Request.wsdl')

result = client.service.GenericAPIRequest(request)
print result
