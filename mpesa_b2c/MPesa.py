from suds.client import Client
from suds.cache import NoCache
from suds.sax.text import Raw
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_v1_5
from mpesa_configs import broker_params, logger
from suds.transport.http import HttpAuthenticated
from suds.transport import Reply, TransportError
from datetime import datetime

import base64
import MySQLdb
import MySQLdb.cursors
import logging
import eventlet
import hashlib
import requests
import xml.etree.ElementTree as ET

logging.basicConfig(level=logging.INFO)
logging.getLogger('suds.client').setLevel(logging.DEBUG)

""" Overide Suds Transport to allow for SSL 
"""
class RequestsTransport(HttpAuthenticated):
    def __init__(self, **kwargs):
        self.cert = kwargs.pop('cert', None)
        HttpAuthenticated.__init__(self, **kwargs)

    def send(self, request):
        self.addcredentials(request)
        resp = requests.post(request.url, data=request.message,
                             headers=request.headers, cert=self.cert, verify=True)
        result = Reply(resp.status_code, resp.headers, resp.content)
        return result

class MPesa():

    def __init__(self):        
        self.count = 1
        self.headers = {'Content-Type': 'text/xml; charset=utf-8'}
        t = RequestsTransport(cert=broker_params['certificate'])
        self.client = Client(broker_params['mpesa_wsdl'], headers=self.headers, timeout=30, transport=t)
        #self.client = Client(broker_params['mpesa_wsdl'], headers=self.headers, timeout=30) 
        self.client.options.location = broker_params['broker_endpoint']

    def getCDATASection(self, service_params, transaction_id, amount, init_encr_pass, msisdn):
        """ 
          Try to generate xml to use for the CDATA section
          Args:
              service_params (dic) : check config
              transaction_id (str) : Merchant transaction ID
              amount (float) : Amount to send to customer 
              init_encr_pass (str) : Encryted Initator Password
              msisdn (str) : String
          Returns:
            xml:
        """
        return Raw("""
        <![CDATA[<request xmlns="http://api-v1.gen.mm.vodafone.com/mminterface/request">
            <Transaction>
               <CommandID>""" + service_params['command_id'] + """</CommandID>
               <LanguageCode>0</LanguageCode>
               <OriginatorConversationID>""" + transaction_id + """</OriginatorConversationID>
               <ConversationID/>
               <Remark>0</Remark>
               <Parameters>
                  <Parameter>
                     <Key>Amount</Key>
                     <Value>""" + str(amount) + """</Value>
                  </Parameter>
               </Parameters>
               <ReferenceData>
                  <ReferenceItem>
                     <Key>QueueTimeoutURL</Key>
                     <Value>""" + broker_params['timeout_endpoint'] + """</Value>
                  </ReferenceItem>
                  <ReferenceItem>
                     <Key>Occasion</Key>
                     <Value>Jamuhuri</Value>
                  </ReferenceItem>
               </ReferenceData>
               <Timestamp>""" + datetime.strftime(datetime.now(), "%Y-%m-%dT%H:%M:%S.%fZ") + """</Timestamp>
            </Transaction>
            <Identity>
               <Caller>
                  <CallerType>2</CallerType>
                  <ThirdPartyID/>
                  <Password>Password0</Password>
                  <CheckSum>CheckSum0</CheckSum>
                  <ResultURL>""" + broker_params['result_endpoint'] + """</ResultURL>
               </Caller>
               <Initiator>
                  <IdentifierType>11</IdentifierType>
                  <Identifier>""" + service_params['initiator'] + """</Identifier>
                  <SecurityCredential>""" + init_encr_pass + """</SecurityCredential>
                  <ShortCode>""" + str(service_params['shortcode']) + """</ShortCode>
               </Initiator>
               <PrimaryParty>
                  <IdentifierType>4</IdentifierType>
                  <Identifier>""" + str(service_params['shortcode']) + """</Identifier>
                  <ShortCode/>
               </PrimaryParty>
               <ReceiverParty>
                  <IdentifierType>1</IdentifierType>
                  <Identifier>""" + str(msisdn) + """</Identifier>
                  <ShortCode/>
               </ReceiverParty>
               <AccessDevice>
                  <IdentifierType>1</IdentifierType>
                  <Identifier>Identifier3</Identifier>
               </AccessDevice>
            </Identity>
            <KeyOwner>1</KeyOwner>
        </request>]]>
         """)
    
    def encrypt_initator_password (self, init_password) :
        """
            Encrypt init_password using RSASSA-PKCS #1 v1.5.
            Base 64 Encode String
            Args:
                init_password (str) : Unencrypted Initiator Password 
            Returns:
                encrypted_password (str)
        """
        pubkey = open(broker_params['public_key']).read()
        key = RSA.importKey(pubkey)
        cipher = PKCS1_v1_5.new(key)
        return base64.b64encode(cipher.encrypt(init_password))

    def encrypt_sp_password (self, timestamp):
        """ 
            SHA 256 Encrypt SP password
            Base 64 Encode string
            Args:
                timestamp (str) : Transaction timestamp
            Returns:
                encrypted_password (str)
        """
        m = hashlib.sha256()
        m.update(broker_params['sp_id'] + broker_params['sp_password'] + timestamp)
        return base64.b64encode(m.hexdigest())

    def add_header(self):
        """ 
           Add header to the M-Pesa XML request
        """
        header = self.client.factory.create('{http://www.huawei.com.cn/schema/common/v2_1}RequestSOAPHeader')
        timestamp = datetime.strftime(datetime.now(), '%Y%m%d%H%M%S')
        header.spId = broker_params['sp_id']
        header.spPassword = self.encrypt_sp_password (timestamp)
        header.serviceId = broker_params['service_id']
        header.timeStamp = timestamp
        self.client.set_options(soapheaders=header)

    def generic_api_request(self, service_params, transaction_id, msisdn, amount):
        """
           Make M-Pesa request
           Args:
               service_params (dict) : 
               transaction_id (str) : Merchant Transaction ID
               amount (float) :
               msisdn (str): Mobile number of the customer receiving M-Pesa funds
           Returns:
               result (dict):
           Raises:
               Exception
        """   
        self.add_header()
        init_encr_pass = self.encrypt_initator_password(service_params['init_password'])
        result = None
        try:
            cdata = self.getCDATASection(service_params, transaction_id, amount, init_encr_pass, msisdn)
            rs = self.client.service.GenericAPIRequest(cdata)
            #Parse Response
            tree = ET.fromstring(rs)
            result = {}
            for item in tree.getiterator():
                if '}' in item.tag:
                    item.tag = item.tag.split('}', 1)[1]  # strip all namespaces
                result[item.tag] = item.text
        except Exception, e:
            logger.error(e)
            raise 
        return result
