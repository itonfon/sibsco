#M-Pesa B2C Daemon
TODO: This is a Python daemon for the Safaricom's M-Pesa API. 
The API allows a merchant to initiate B2C transactions via an API. 
The merchant submits authentication details, transaction details and call back url .
After request submission, the merchant receives instant feedback with the transaction ID. The transaction is processed and its status is made through a callback url.

## Installing certificate
1. Generate CSR and send to Safaricom for signing
2. Extract public key from private key - openssl rsa -in privkey.pem -pubout > key.pub
3. Extract the certificates from p7b file - openssl pkcs7 -inform DER -outform PEM -in test.p7b -print_certs > test.cer
4. Add intermediateCA.cer, private_key (remove pass phrase) and ProdBroker.cer or TestBroker.cer to the bundles

## Running Daemon
* Copy Daemon to /usr/local/lib/
* Modify mpesa.conf to your needs
* Run Mpesa B2C Daemon on supervisor 
## License
