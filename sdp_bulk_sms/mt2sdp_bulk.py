#!/usr/bin/python

from sdp_bulk import SDP_Queue_MySQL, SDP
import time
import datetime
import logging
import ConfigParser
from sdp_configs import logger, config, mysql_params 
from functools import partial
import os
import signal
import MySQLdb
import MySQLdb.cursors
import sys         
import ConfigParser
import eventlet
eventlet.monkey_patch()

is_sending = 0
is_waiting_to_die = 0
#sdp = SDP()
sdp_queue_mysql = SDP_Queue_MySQL()

def signal_handler(signum, frame):
    global is_sending, is_waiting_to_die
    logger.info('SIGNAL:\tSIGTERM')
    if is_sending:
        is_waiting_to_die = 1
        logger.info('SENDING...\t WAIT!')
        return 
    logger.info('DIE')
    exit(0) 

def service_params(service):
    sections = config.sections()
    try:
        index = sections.index(service)
        name = config.get(sections[index],'name')
        service_id = config.get(sections[index],'service_id')
        access_no = config.get(sections[index],'access_no')
        correlator = config.get(sections[index],'correlator')
        params = {'name':name, 'service_id':service_id, 
                'access_no':access_no, 'correlator':correlator}
        return params
    except Exception, e:
        raise

def service_init(args):
    """ Defines how the application is started.
    args = terminal arguments used to run the service """
    
    app_list = config.sections()
    reserved_sections = ['sdp', 'mysql', 'logger']

    #Invalid Arguments or Invalid Params
    if len(args) < 2:
        logger.error("USAGE: python %s <app_name>" % args[0])         
        exit(0)
    elif sys.argv[1] not in app_list:
        logger.error('Invalid Args: App %s must be in the config file' % args[1])
        exit(0)
    elif args[1] in reserved_sections:
        logger.error('Invalid Args: %s is a reserved section' % args[1])
        exit(0)
    else:
        try:
            params = service_params(args[1])
            logger.info('%s: %s Started! Name: %s, Correlator : %s, Access No : %s' 
                    % (datetime.datetime.now(), args[1], params['name'], \
                            params['correlator'], params['access_no']))
            return params
        except Exception, e:
            logger.error('Invalid Params in the configuration file. %s' % e)
            exit(0)
  
def _fetch_from_queue(access_no=None, connection=None):
    """ Retrieve from messages from the database    
    Params:
        direction --- IN for message direction IN and OUT for OUT   
    """
    
    sql = """ SELECT RecordID, Originator, Destination, Message, 
              MessageTimeStamp, MessageDirection, SMSCID 
              FROM dbQueue WHERE MessageDirection = %s AND Originator = %s LIMIT 1000
            """
    params = ('OUT', access_no, )
    try:
        msg_list = sdp_queue_mysql.retrieve_all_data_params(connection, sql, params)
        return msg_list
    except Exception, e:
        logger.error(e)
        raise
 
def _delete_msg(record_id, connection):
    del_sql = 'DELETE FROM dbQueue WHERE RecordID = %s '    
    params = (int(record_id),)
    try:
        sdp_queue_mysql.execute_query(connection, del_sql, params)
        return True
    except Exception, e:
        logger.error('_delete_msg: %s' % e)
        raise    
          
def _push_to_cdr(msg, correlator, request_id, connection):
    sql = """INSERT INTO dbCDR(Originator, Destination, Message, 
             MessageTimeStamp, SMSCID,
             RequestIdentifier, Correlator) 
	     VALUES(%s, %s, %s, %s, %s, %s, %s)"""
    timestamp = datetime.datetime.strftime(datetime.datetime.now(), '%Y%m%d%H%M%S')
    blank_msg = "" # Don't log message text

    params = (msg['Originator'], msg['Destination'], msg['Message'], \
                timestamp, msg['SMSCID'], request_id, correlator)
    try:
        sdp_queue_mysql.execute_query(connection, sql, params)
        return True
    except Exception, e:
        logger.error(e)
        raise


def dispatch(service_params, connection):
    """ Pops sms from queue, sends it destination, record transaction to cdr 
    """
    global is_sending, is_waiting_to_die
    access_no = service_params['access_no'] 
    service_id = service_params['service_id']
    sender_name = service_params['correlator']
    
    msg_list = _fetch_from_queue(access_no=access_no, connection=connection)
    if not msg_list:
        return

    msgs = []
    msgs.append(msg_list)    

    try:
        pool_size = 5
        is_sending = True
        pool = eventlet.GreenPool(size=pool_size)

        for response_list in pool.imap(partial(process_msg, connection, service_id), msgs):
            for response in response_list:
                logger.info('RESP: Correlator:%s, RequestID:%s, Destination:%s' %\
                        (response['correlator'], response['request_id'],
                         response['destination']))
        
        is_sending = False
        if is_waiting_to_die:
            logger.info('NOT SENDING:\tDIE.')
            exit(0)
    except Exception, e:
        logger.error(e)
        connection.rollback()
        is_sending = 0
        if is_waiting_to_die:
            logger.info('NOT SENDING:\tDIE.')
            exit(0)
    finally:
        try:
            connection.close()
        except Exception ,e:
            logger.error("MySQL Error closing connection : %s" % e)      

def ascii_msg(msg):
    if isinstance(msg, unicode):
        return msg.encode('ascii', 'ignore')
    #Get the dbencoding
    is_utf8 = False
    try:
        msg = msg.decode('latin1')
    except Exception, e:
        logger.error(e)
        is_utf8 = True

    if is_utf8:
        msg = msg.decode('utf-8')

    return msg.encode('ascii', 'ignore')

def process_msg(connection, service_id, msg_list):
    if not len(msg_list):
        return [{'correlator':None, 'request_id':None}]
    sdp = SDP()
    #send_access_no = '701291'
    try:
        response_list = []
        for msg in msg_list:
            try:
                #logger.info('DELETE:%s\tID:%s' % (msg['Destination'],\
                #        msg['RecordID']))
                _delete_msg(msg['RecordID'], connection)
                
                destination = "tel:" + msg['Destination']
                link_id = None
                message = ascii_msg(msg['Message'])

                logger.info('REQ: DESTINATION:%s\tOriginator:%s'\
                        %(destination, msg['Originator']))

                response = sdp.send_sms(message, destination,\
                    msg['Originator'], link_id, service_id)
                correlator = response['correlator']
                request_id = response['request_id']
                #logger.info("CORRELATOR:%s\tREQUESTID:%s" % \
                #        (correlator, request_id))
                if not request_id:
                    raise  Exception("NO REQUEST ID", "NO REQUEST ID AFTER sendSms")
                _push_to_cdr(msg, correlator, request_id, connection)
                connection.commit()
                response_list.append(response)
            except Exception, e:
                logger.error(e)
                connection.rollback()
        return response_list
    except Exception, e:
        logger.error(e)

def create_connection():
    """ Creates a connection and returns the connection """
    try:
        connection = MySQLdb.connect(host=mysql_params['host'],\
                user=mysql_params['user'], passwd=mysql_params['passwd'],\
                db=mysql_params['db'], cursorclass=MySQLdb.cursors.DictCursor)
    except MySQLdb.Error, e:
        logger.error(e)
        raise
    return connection

if __name__=='__main__':
    service_params = service_init(sys.argv)
    signal.signal(signal.SIGTERM, signal_handler)

    while True:
        try:
            connection = create_connection() 
            dispatch(service_params, connection)
        except MySQLdb.Error, e:
            logger.error("MySQL Error: %s" % e)
        finally:
            try:
                connection.close()
            except Exception ,e:
                logger.error("MySQL Error closing connection : %s" % e) 
        time.sleep(1)
