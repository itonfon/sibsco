import logging                                                                                 
import ConfigParser                                                                            
                                                                                               
CONFIG_FILE = r'/usr/local/lib/sdp_bulk_sms/sdp.conf'                            
config = ConfigParser.ConfigParser()                                                           
config.read(CONFIG_FILE)                                                                       

#Get the global details
access_file = config.get("sdp", "access_file")
access_config = ConfigParser.ConfigParser()
access_config.read(access_file)

#logging.basicConfig(level=logging.INFO)                                                        
logger = logging.getLogger("SDP")                                                              
logger.setLevel(logging.DEBUG)
#logger.setLevel(logging.INFO)
#hdlr = logging.FileHandler(config.get("logger", "log_file"))                                   
hdlr = logging.StreamHandler()                                   
formatter = logging.Formatter('%(asctime)s %(name)s %(levelname)s %(message)s')                         
hdlr.setFormatter(formatter)                                                                   
logger.addHandler(hdlr)                                                                        
                                                                                                                                                           
host = config.get("mysql", "host")                                                             
port = config.get("mysql", "port")                                                             
user = config.get("mysql", "user")                                                             
passwd = config.get("mysql", "password")                                                       
db = config.get("mysql", "database")                                                           
connection_timeout = config.get("mysql", "connection_timeout")                                 
mysql_params = {
        'host':host,
        'port':port,
        'user':user,
        'passwd':passwd,
        'db':db,
        'connection_timeout':connection_timeout        
        }

sdp_wsdl = config.get("sdp", "sdp_wsdl")                                                       
endpoint = config.get("sdp", "endpoint")                                                       
send_location = config.get("sdp", "send_location")                                  
sp_id = access_config.get("sdp_bulk", "sp_id")                                                  
password = access_config.get("sdp_bulk", "password")
dlr_send_location = config.get('sdp', 'send_location')
sdp_params = {                                                                          
    'sdp_wsdl':sdp_wsdl,
    'endpoint':endpoint,                                                                
    'send_location':send_location,                                                      
    'sp_id':sp_id,                                                                      
    'password':password,
    'send_location_dlr':dlr_send_location,
}                                                                                       
