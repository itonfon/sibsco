#!/usr/bin/python

from datetime import datetime
from suds.client import Client
from suds.sax.element import Element
import hashlib
import logging
import MySQLdb
from sdp_configs import sdp_params, logger
import eventlet
eventlet.monkey_patch()

class SDP_Queue_MySQL():               
        
    def execute_query(self, db_connection, sql, params):
        """ Insert data into the database
        Keyword arguments:
            db_connection -- The connection to the database.
            sql -- The sql to be executed.
            params -- A parameter list holding values to be used in the sql.        
        """
        try:
            cursor = db_connection.cursor()
            cursor.execute(sql, params)
        except MySQLdb.DatabaseError:
            raise
        except TypeError:
            raise

    def retrieve_all_data_params(self, db_connection, sql, params):
        """ Retrieve Data from the database
        
            Retrieve data from the database, returns a dictionary that 
            holds the results.
        
            Keyword arguments:
            db_connection -- The connection to the database.
            sql -- The sql to be executed.
            params -- A parameter list holding values to be used in the sql.
        
        """
        results = None
        try:
            cursor = db_connection.cursor()
            cursor.execute(sql, params)
            results = cursor.fetchall()
        except MySQLdb.DatabaseError:
            raise
        return results
    
    def retrieve_all_data(self, db_connection, sql):
        """ Retrieve Data from the database
        
            Retrieve data from the database, returns a dictionary that 
            holds the results.
        
            Keyword arguments:
            db_connection -- The connection to the database.
            sql -- The sql to be executed.
        
        """
        results = None
        try:
            cursor = db_connection.cursor()
            cursor.execute(sql)
            results = cursor.fetchall()
        except MySQLdb.DatabaseError:
            raise
        return results

class SDP():

    def __init__(self):        
        self.count = 1
        self.headers = {'Content-Type': 'text/xml; charset=utf-8'}
        self.client = Client(sdp_params['sdp_wsdl'], headers=self.headers,\
                timeout=30) 
        self.client.options.location = sdp_params['send_location']
        

    def set_soap_values(self, originator, destination, link_id, service_id):
        # Define Receipt request
        self.rr = self.client.factory.create('ns0:SimpleReference')
        self.rr.endpoint = sdp_params['endpoint']
        
        self.rr.interfaceName = 'SmsNotification'

        #Generate a correlator
        self.access_no = destination
        ts = datetime.strftime(datetime.now(), '%y%m%d%H%M%S%f')
        

        if str(self.count) == (str(9)*9):
            self.count = 1
        self.correlator = str(self.access_no) + service_id +\
            ts + str(self.count).zfill(9) 
        self.rr.correlator = self.correlator
        self.count += 1 #Increment the count

        # Hash password
        timestamp = datetime.strftime(datetime.now(), '%Y%m%d%H%M%S')
        sp_id = sdp_params['sp_id']
        service_id = service_id
        password =  sdp_params['password']
        m = hashlib.md5()
        m.update(sp_id + password + timestamp)
        sp_password = m.hexdigest()
        self.headerns = ('v2', 'http://www.huawei.com.cn/schema/common/v2_1')
        spIdElement = Element('spId', ns=self.headerns).setText(sp_id)
        spPasswordElement = Element('spPassword', ns=self.headerns).setText(sp_password)
        serviceIdElement = Element('serviceId', ns=self.headerns).setText(service_id)
        timestampElement = Element('timeStamp', ns=self.headerns).setText(timestamp)
        linkId = Element('linkid', ns=self.headerns).setText(link_id)
        oaElement = Element('OA', ns=self.headerns).setText(originator)
        faElement = Element('FA', ns=self.headerns).setText(originator)

        # Create the SOAP header
        requestSOAPHeader = Element('RequestSOAPHeader', ns=self.headerns)
        requestSOAPHeader.insert(faElement)
        requestSOAPHeader.insert(oaElement)
        requestSOAPHeader.insert(linkId)
        requestSOAPHeader.insert(timestampElement)
        requestSOAPHeader.insert(serviceIdElement)
        requestSOAPHeader.insert(spPasswordElement)
        requestSOAPHeader.insert(spIdElement)

        self.client.set_options(soapheaders=requestSOAPHeader)

    def send_sms(self, msg, originator, destination, link_id, service_id):
        """ Sends a message and returns the a dictionary containing:
                 correlator and the result (requestIdentifier) used to send the message 
                else returns None
        """
        if msg == None:
            raise Exception("Empty Message", "Cannot send an empty message.")
        
        #set soap_header
        self.set_soap_values(originator, destination, link_id, service_id)   
        result = None
        try:
            result = self.client.service.sendSms(originator, destination, None, msg, self.rr)
        except Exception, e:
            logger.error(e)
            raise 
        response = {'correlator':self.correlator, 'request_id':result, 'destination':originator}        
        return response
