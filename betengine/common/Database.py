from MySQL import MySQL
from sibsco_configs import logger

mysql = MySQL()

class Database:

    def __init__(self, conn):
        self.conn = conn

    def get_pending_transactions(self, limit):
        """Get pending Mpesa messages.
        Args:
            limit (int): No. of messages to retrieve.
        Returns:
            list: A list of messages for processing.
        """
        sql = """SELECT id, tran_id, tran_time, business_short_code, account_no,\
                msisdn, balance, amount, name, description, date_created,\
                'SAFCOM_KE' as SMSCID
                FROM mpesa_c2b_confirmation LIMIT %s
          """
        params = (limit, )
        try:
            trx_list = mysql.retrieve_all_data_params(self.conn, sql, params)
            return trx_list
        except Exception, e:
            logger.error(e)
            raise

    def _delete_transaction(self, id):
        del_sql = 'DELETE FROM mpesa_c2b_confirmation WHERE id = %s '
        params = (int(id),)
        try:
            mysql.execute_query(self.conn, del_sql, params)
            return True
        except Exception, e:
            logger.error('_delete_transaction: %s' % e)
            raise

    def _push_to_processed(self, mpesa_trx):
        insert_sql = """INSERT INTO mpesa_c2b_complete
                    (tran_id, tran_time, business_short_code, account_no, msisdn,
                    balance, amount, name, description, date_created)
                    VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"""

        params = (mpesa_trx['tran_id'], mpesa_trx['tran_time'], mpesa_trx['business_short_code'],
               mpesa_trx['account_no'], mpesa_trx['msisdn'], mpesa_trx['balance'], mpesa_trx['amount'],
               mpesa_trx['name'],mpesa_trx['description'],mpesa_trx['date_created'],)
        try:
            mysql.execute_query(self.conn, insert_sql, params)
            return True
        except Exception, e:
            logger.error(e)
        raise

    def queue_sms(self, originator, destination, message, smsc_id):
        """Adds an outgoing message to be sent out as SMS.
        Useful to send out processed ticket + message to
        the subscriber.
        """
        sql = """INSERT INTO dbQueue
                (Originator, Destination, Message, MessageTimeStamp, MessageDirection, SMSCID)
                VALUES(%s, %s, %s, NOW(), %s, %s)"""

        params = (originator, destination, message, 'OUT', smsc_id, )
        try:
            mysql.execute_query(self.conn, sql, params)
            return True
        except Exception, e:
            logger.error(e)
            raise

    def log_purchased_ticket(self, user_id, amount, game_id):

        """Log purchased ticket"""
        sql = """INSERT INTO ticket_log
                (user_id, game_id, amount, date_issued)
                VALUES(%s, %s, %s, NOW())"""

        params = (user_id, game_id, amount, )
        try:
            id = mysql.execute_query_return_id(self.conn, sql, params)
            return id
        except Exception, e:
            logger.error(e)
            raise


    def log_match_prediction_ticket(self, ticket_id, ticket_no):
        values = []
        param_list = []
        for i, ticket in enumerate(ticket_no.split(','), start=1):
            values.append('(%s, %s, %s, NOW())')
            param_list.append(ticket_id)
            param_list.append(i)
            param_list.append(ticket)

        """Log purchased ticket"""
        sql = """INSERT INTO match_ticket
                (ticket_id, odd_index, odd, date_issued)
                VALUES %s""" %(",".join(values))

        params = tuple(param_list)
        try:
            mysql.execute_query(self.conn, sql, params)
        except Exception, e:
            logger.error(e)
            raise

    def get_pending_requests(self, limit):
        """Get pending Mpesa messages.
        Args:
            limit (int): No. of messages to retrieve.
        Returns:
            list: A list of messages for processing.
        """
        sql = """SELECT RecordID, Originator, Destination, Message, MessageTimeStamp, \
                    MessageDirection, SMSCID\
                    FROM dbQueue WHERE MessageDirection = %s LIMIT %s """
        params = ('IN', limit)
        try:
            req_list = mysql.retrieve_all_data_params(self.conn, sql, params)
            return req_list
        except Exception, e:
            logger.error(e)
            raise

    def _delete_record(self, record_id):
        del_sql = """DELETE FROM dbQueue WHERE RecordID = %s"""
        params = (record_id,)
        try:
            mysql.execute_query(self.conn, del_sql, params)
            return True
        except Exception, e:
            logger.error('_delete_transaction: %s' % e)
            raise

    def _push_to_cdr(self, request):
        sql = """INSERT INTO dbCDR (Originator, Destination, Message, MessageTimeStamp, \
                 SMSCID) \
                 VALUES (%s, %s, %s, %s, %s)"""
        params = (request['Originator'], request['Destination'],
                      request['Message'], request['MessageTimeStamp'],
                      request['SMSCID'])
        try:
            mysql.execute_query(self.conn, sql, params)
            return True
        except Exception, e:
            logger.error('_push_to_cdr: %s' % e)
            raise


    def _fetch_user_details(self, msisdn):
        """
        """
        results = None
        sql = """SELECT id, msisdn, balance, name \
                 FROM `customer` WHERE msisdn = %s LIMIT 1""";
        params = (msisdn,)
        try:
            results = mysql.retrieve_row_params(self.conn, sql, params)
            return results
        except Exception, e:
            logger.error(e)
            raise

    def _fetch_game_details(self, last_draw_date, next_draw_date):
        """
        """
        results = None
        sql = """SELECT id, description, bet_limit, draw_start_date, draw_end_date\
                 FROM `game` WHERE draw_start_date between %s  AND %s ORDER BY draw_start_date ASC LIMIT 1""";
        params = (last_draw_date, next_draw_date, )
        try:
            results = mysql.retrieve_row_params(self.conn, sql, params)
            return results
        except Exception, e:
            logger.error(e)
            raise

    def _register_user (self, msisdn, balance):

        sql = """INSERT INTO customer
                (msisdn, balance, date_created) \
                VALUES(%s, %s, NOW())""";
        params = (msisdn, balance)
        try:
            mysql.execute_query(self.conn, sql, params)
            return True
        except Exception, e:
            logger.error(e)
            raise

    def _update_user_balance(self, msisdn, name, balance):
        """
        """
        sql = """UPDATE customer SET balance = %s, name = %s \
                WHERE msisdn = %s"""
        params = (balance, name, msisdn)
        try:
            mysql.execute_query(self.conn, sql, params)
            return True
        except Exception, e:
            logger.error(e)
            raise

    def _mpesa_disburse(self, msisdn, amount):
        """
        """
        sql = """INSERT INTO mpesa_disburse (transaction_id, msisdn, amount, date_created)
                     VALUES(UUID(), %s, %s, NOW())"""
        params = (msisdn, amount)
        try:
            mysql.execute_query(self.conn, sql, params)
            return True
        except Exception, e:
            logger.error(e)
            raise

    def queue_sms(self, originator, destination, message, smsc_id):
        """Adds an outgoing message to be sent out as SMS.
           Useful to send out processed ticket + message to
           the subscriber.
        """
        sql = """INSERT INTO dbQueue
            (Originator, Destination, Message, MessageTimeStamp, MessageDirection, SMSCID)
            VALUES(%s, %s, %s, NOW(), %s, %s)"""

        params = (originator, destination, message, 'OUT', smsc_id, )
        try:
            mysql.execute_query(self.conn, sql, params)
            return True
        except Exception, e:
            logger.error(e)
            raise

    def _purchased_ticket_count(self, user_id, date_last_draw):
        results = None
        sql = """SELECT count(ticket_id) as count FROM ticket_log
                    WHERE user_id = %s AND date_issued > %s """;
        params = (user_id, date_last_draw )
        try:
            results = mysql.retrieve_row_params(self.conn, sql, params)
            return results['count']
        except Exception, e:
            logger.error(e)
            raise

    def log_customer_transaction (self, msisdn, mno_trx_id, description,dr_amount,cr_amount, prev_bal, new_bal):
        sql = """INSERT INTO customer_trx_log (msisdn, mno_trx_id, description, dr, cr, prev_bal, new_bal, date_created) \
                    VALUES (%s, %s, %s, %s, %s, %s, %s, NOW())"""
        params = ( msisdn, mno_trx_id, description, dr_amount, cr_amount, prev_bal, new_bal)
        try:
            mysql.execute_query(self.conn, sql, params)
            return True
        except Exception, e:
            logger.error(e)
            raise

    def log_system_transaction (self, msisdn, mno_trx_id, description, dr_amount, cr_amount):
        sql = """INSERT INTO system_trx_log (msisdn, mno_trx_id, description, dr, cr, date_created) \
                    VALUES (%s, %s, %s, %s, %s, NOW())"""
        params = ( msisdn, mno_trx_id, description, dr_amount, cr_amount)
        try:
            mysql.execute_query(self.conn, sql, params)
            return True
        except Exception, e:
            logger.error(e)
            raise

    def _new_game(self, description, bet_limit, draw_start_date, draw_end_date):
        """
        """
        results = None
        sql = """INSERT INTO game (description, bet_limit, draw_start_date, draw_end_date, status)
                 VALUES (%s, %s, %s, %s, %s)""";
        params = (description, bet_limit, draw_start_date, draw_end_date, '0')
        try:
            id = mysql.execute_query_return_id(self.conn, sql, params)
            return id
        except Exception, e:
            logger.error(e)
            raise
