import MySQLdb
import MySQLdb.cursors
import logging

from sibsco_configs import logger, config, mysql_params 

class MySQL():               
        
    def execute_query(self, db_connection, sql, params):
        """ Insert data into the database
        Keyword arguments:
            db_connection -- The connection to the database.
            sql -- The sql to be executed.
            params -- A parameter list holding values to be used in the sql.        
        """
        try:
            cursor = db_connection.cursor()
            cursor.execute(sql, params)
        except MySQLdb.DatabaseError:
            raise
        except TypeError:
            raise
    
    def execute_query_return_id(self, db_connection, sql, params):
        """ Insert data into the database
            Keyword arguments:
            db_connection -- The connection to the database.
            sql -- The sql to be executed.
            params -- A parameter list holding values to be used in the sql.
        """
        try:
            cursor = db_connection.cursor()
            cursor.execute(sql, params)
	    last_insert_id = cursor.lastrowid 
        except MySQLdb.DatabaseError:
            raise
        except TypeError:
            raise
	return last_insert_id
	
    def retrieve_all_data_params(self, db_connection, sql, params):
        """ Retrieve Data from the database
        
            Retrieve data from the database, returns a dictionary that 
            holds the results.
        
            Keyword arguments:
            db_connection -- The connection to the database.
            sql -- The sql to be executed.
            params -- A parameter list holding values to be used in the sql.
        
        """
        results = None
        try:
            cursor = db_connection.cursor()
            cursor.execute(sql, params)
            results = cursor.fetchall()
        except MySQLdb.DatabaseError:
            raise
        return results
    
    def retrieve_all_data(self, db_connection, sql):
        """ Retrieve Data from the database
        
            Retrieve data from the database, returns a dictionary that 
            holds the results.
        
            Keyword arguments:
            db_connection -- The connection to the database.
            sql -- The sql to be executed.
        
        """
        results = None
        try:
            cursor = db_connection.cursor()
            cursor.execute(sql)
            results = cursor.fetchall()
        except MySQLdb.DatabaseError:
            raise
        return results
    
    def retrieve_row_params(self, db_connection, sql, params):
        """ Retrieve Data from the database

            Retrieve data from the database, returns a dictionary that 
            holds the first row of the results.

            Keyword arguments:
            db_connection -- The connection to the database.
    	    sql -- The sql to be executed.
    	    params -- A parameter list holding values to be used in the sql.

    	"""
    	result = None
    	try:
            cursor = db_connection.cursor()
            cursor.execute(sql, params)
            result = cursor.fetchone()
    	except MySQLdb.DatabaseError: 
            raise
    	return result
    def retrieve_row(self, db_connection, sql):
        """ Retrieve Data from the database

            Retrieve data from the database, returns a dictionary that
            holds the first row of the results.

            Keyword arguments:
            db_connection -- The connection to the database.
            sql -- The sql to be executed.
            params -- A parameter list holding values to be used in the sql.

        """
        result = None
        try:
            cursor = db_connection.cursor()
            cursor.execute(sql)
            result = cursor.fetchone()
        except MySQLdb.DatabaseError:
            raise
        return result
