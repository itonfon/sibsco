import re
import datetime
import os
import random

from sibsco_configs import logger, sms_templates, defaults

class Engine:

    def register_user(self, db, msisdn, msgtxt, balance):
        """
        Try to register a user to the service
        Checks if user msisdn exists on the database
        Args:
            db (Obj): Database object
            msisdn (Str): Customer Mobile Number
            msgtxt (Str): Message
            balance (Float): Customer balance as at registration
        Returns:
            Response (Str) : Message to be sent back to the customer
        """
        user = db._fetch_user_details(msisdn)
        if user is not None:
            response = sms_templates['user_exists']
        else:
            db._register_user(msisdn, balance)
            response = sms_templates['welcome_sms']

        return response


    def check_balance(self, db, msisdn, msgtxt):
        """ Check customer wallet balance
            Args:
                db (Obj): Database object
                msisdn (Str): Customer Mobile Number
                msgtxt (Str): Message
            Returns:
                Response (Str) : Message to be sent back to the customer
        """
        user = db._fetch_user_details(msisdn)
        if user is not None:
            response = sms_templates['bal_resp'] %(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), user['balance'])
        else:
            response = sms_templates['not_registered']
        return response


    def deposit(self, db, msisdn, name, amount, tran_id):
        """ Tries to deposit funds into customer wallet account
            if user is not registered registers user and deposits funds into their wallet
            Args:
                db (Obj): Database object
                msisdn (Str): Customer Mobile Number
                name (Str): Customer name as received from the MNO
                amount (Float) : Amount Deposited
            Returns:
                Response (Str) : Message to be sent back to the customer (via BULK SMS)
        """
        user = db._fetch_user_details(msisdn)
        prev_bal = 0.00

        if user is not None:
            prev_bal = user['balance']
            balance = user['balance'] + amount
            db._update_user_balance(user['msisdn'], name, balance)
        else:
            balance = amount
            self.register_user(db, msisdn, '', balance)

        db.log_customer_transaction(msisdn, tran_id, 'Customer Deposit ' + msisdn, 0, amount, prev_bal, balance)

        response = sms_templates['deposit_success'] %(amount, balance)
        return response


    def withdraw(self, db, msisdn, msgtxt):
        """ Try to withdraw funds from customer wallet
            Check if user balance supports the withdrawal
            Args:
                db (Obj): Database object
                msisdn (Str): Customer Mobile Number
                msgtxt (Str): Message
            Returns:
                Response (Str) : Message to be sent back to the customer
        """
        msgtxt = re.sub(r'-|\||\$|\*|\.|,', '#', msgtxt)
        request = msgtxt.split("#")
        user = db._fetch_user_details(msisdn)
        if user is not None:
            if len(request) >= 2 and request[1].isdigit() :
                amount = int(request[1])
                prev_bal = user['balance']
                balance = user['balance']
                if balance >= amount:
                    balance -= amount
                    db._update_user_balance(user['msisdn'], user['name'], balance)
                    db._mpesa_disburse(user['msisdn'], amount)
                    db.log_customer_transaction(user['msisdn'], '', 'Withdraw' , amount, 0, prev_bal, balance)
                    response = sms_templates['withd_success'] %(amount, balance)
                else:
                    response = sms_templates['low_withd_bal'] %(amount, user['balance'])
            else:
                response = sms_templates['invalid_command'] %('To withdraw from your wallet', 'Withdraw#Amount')
        else:
            response = sms_templates['not_registered']
        return response

    def issue_match_ticket(self, db, msisdn, msgtxt):
        """ Parse or generate tickets for a subscriber based on amount received.
            The method makes use of `parse` and `generate` to create the tickets.
            - Generated tickets are saved as messages to be sent out to the subscriber.
            - Generated tickets are also logged for reference
            If the amount for the ticket is insufficient, it's returned without issuing any tickets.
            Args:
                amount (int): Amount received from subscriber.
                msisdn (str)
                message (str): Message from subscriber containing ticket.
            Returns:
                Str : response to send to the subscriber
        """

        request = msgtxt.split('#')
        ticket = request[1] if len(request) >= 2 else ""
        user = db._fetch_user_details(msisdn)
        draw_time = defaults['draw_time'].split(',')
        last_draw_date = self.last_draw_date()
        next_draw_date = self.next_draw_date()

        game = db._fetch_game_details(last_draw_date, next_draw_date)

        bet_limit = defaults['bet_limit']
        if game is None:
            game_id  = db._new_game('Game ID', bet_limit, last_draw_date, next_draw_date)
        else:
            game_id = game['id']
            bet_limit = game['bet_limit']

        if int(bet_limit) > 0 and db._purchased_ticket_count(user['id'], last_draw_date) > int(bet_limit):
            response = sms_templates['ticket_limit_exceed']
        else:

            if user is not None:
                bet_amount = int(defaults['bet_amount'])
                prev_bal = user['balance']
                balance = user['balance']
                if balance >= bet_amount:
                    #in case any games are zero rated
                    if (bet_amount > 0):
                        balance -= int(defaults['bet_amount'])
                        db._update_user_balance(user['msisdn'], user['name'], balance)

                    next_draw = self.next_draw_date()
                    ticket = self.parse_or_generate_ticket(ticket)
                    #log generated ticket
                    ticket_id = db.log_purchased_ticket(user['id'], bet_amount, game_id)

                    db.log_match_prediction_ticket(ticket_id, ticket)
                    db.log_customer_transaction(msisdn, ticket_id, 'Placed Bet', bet_amount, 0, prev_bal, balance)
                    db.log_system_transaction(msisdn, ticket_id, 'Placed Bet', 0, bet_amount)
                    #generate ticket message
                    response = sms_templates['ticket_sms'] % (ticket, next_draw.strftime("%a %d. %b %Y"), "%011d" % ticket_id, str(balance))
                else:
                    response = sms_templates['low_balance'] %(user['balance'])
            else:
                response = not_registered['not_registered']
        return response

    def next_draw_date(self):
        """ Get the next draw Date
            # 0 = Monday, 1=Tuesday, 2=Wednesday, 3 = Thursday 4 = Friday 5 Sartuday = 6
            Args:
                None
            Return:
                Date: next date draw will be held
        """
        today = datetime.date.today()
        day_of_week = today.weekday()
        draw_date = map(int, defaults['draw_date'].split(','))
        draw_time = defaults['draw_time'].split(',')
        if day_of_week in draw_date:
            if  datetime.datetime.now().time() >= datetime.time(int(draw_time[0]), int(draw_time[1])):
                weekday = draw_date[1] if draw_date[0] == day_of_week else draw_date[0]
                #next_draw = self.date_weekday(today, weekday, True).strftime("%a %d. %b %Y")
                next_draw = datetime.datetime.combine(self.date_weekday(today, weekday, True), datetime.time(int(draw_time[0]), int(draw_time[1])))
            else:
                #next_draw = today.strftime("%a %d. %b %Y")
                next_draw = datetime.datetime.combine(today, datetime.time(int(draw_time[0]), int(draw_time[1])))
        else:
            weekday = draw_date[1] if day_of_week > draw_date[0] and day_of_week < draw_date[1] else draw_date[0]
            #next_draw = self.date_weekday(today, weekday, True).strftime("%a %d. %b %Y")
            next_draw = datetime.datetime.combine(today, datetime.time(int(draw_time[0]), int(draw_time[1])))
        return next_draw

    def date_weekday(self, d, weekday, future):
        """ Get next weekday provided the date and the weekday
            Args:
                d(date) : Date
                weekday(int) :
                future (boolean) : get a date in the future when false date is in the paste
            Return:
                Date:
        """
        if future:
            days = weekday - d.weekday()
            if days <= 0: # Target day already happened this week
                days += 7
            date_requested =  d + datetime.timedelta(days)
        else:
            days = d.weekday() - weekday
            if days <= 0: # Target day was in the last week
                days += 7
            date_requested = d - datetime.timedelta(days)
        return date_requested

    def last_draw_date(self):
        """ Get the next draw Date
            Assumes draws are held Wednesday and Saturday of every week at 21:00
            Args:
                None
            Return:
                Date: next date draw will be held
        """

        today = datetime.date.today()
        draw_date = map(int, defaults['draw_date'].split(','))
        day_of_week = today.weekday()
        draw_time = defaults['draw_time'].split(',')
        if day_of_week in draw_date:
            if datetime.datetime.now().time() >= datetime.time(int(draw_time[0]), int(draw_time[1])):
                last_draw_date = today
            else:
                weekday = draw_date[1] if draw_date[0] == day_of_week else draw_date[0]
                last_draw_date = self.date_weekday(today, weekday, False)
        else:
            weekday = draw_date[0] if day_of_week > draw_date[0] and day_of_week < draw_date[1] else draw_date[1]
            last_draw_date = self.date_weekday(today, weekday, False)
        return datetime.datetime.combine(last_draw_date, datetime.time(int(draw_time[0]), int(draw_time[1])))

    def parse_or_generate_ticket(self, subscriber_generated_ticket):
        """Try to extract a ticket from the subscriber's message.
        Args:
            subscriber_generated_ticket (str): The ticket provided by the subscriber.
        Raises:
            ValueError: If `subscriber_generated_ticket` is not a valid lotto number.
        Returns:
            str: valid ticket.
        """
        #Regex replace all #-|\$
        subscriber_generated_ticket = re.sub(r'#|-|\||\$|\*|\.|,', '', subscriber_generated_ticket)
        ticket = list(subscriber_generated_ticket)
        valid_predict_list = list(defaults['valid_prediction'])
        #check the ticket length
        if len(ticket) > int(defaults['ticket_length']):
            del ticket[defaults['ticket_length']:len(ticket)]
        elif len(ticket) < int(defaults['ticket_length']):
            while len(ticket) < int(defaults['ticket_length']):
                ticket.append('')

        return self.validate_ticket(ticket, valid_predict_list)

    def validate_ticket(self, ticket, valid_predict_list):
        """
        Validates user prediction on matches and generates valid entry for invalid entries
        Args:
            ticket (List) : List with match predictions
            valid_predict_list(List): allowed odds
        Returns
            ticket (String) : Valid ticket for the user
        """

        for index, prediction in enumerate(ticket):
            if valid_predict_list.count(prediction) == 0:
                ticket[index] = random.choice(valid_predict_list)
        return  ",".join(ticket)
