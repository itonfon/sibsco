#!/usr/bin/python

import time
import datetime
import logging
import MySQLdb
import MySQLdb.cursors
import signal
import re
import sys

from lotto.Engine import Engine
from common.Database import Database
from sibsco_configs import logger, config, mysql_params, sms_templates, defaults, keyword, sibsco


is_processing = 0
is_waiting_to_die = 0

def usage():
    """
    Defines how to use this packege
    To start this application supply param arguments python filename.py system-type
    e.g
    python prize.py predict
    """
    help_str = 'USAGE: python %s <system-type>' % sys.argv[0]
    if len(sys.argv) < 2:
        logger.error(help_str)
        exit(0)
    elif sys.argv[1] not in ['sms', 'mpesa']:
        logger.error(help_str)
        exit(0)


def signal_handler(signum, frame):
    global is_processing, is_waiting_to_die
    logger.info('SIGNAL:\tSIGTERM')
    if is_processing:
        is_waiting_to_die = 1
        logger.info('SENDING...\t WAIT!')
        return
    logger.info('DIE')
    exit(0)

def create_connection():
    """ Creates a connection and returns the connection """
    try:
        connection = MySQLdb.connect(host=mysql_params['host'],\
                user=mysql_params['user'], passwd=mysql_params['passwd'],\
                db=mysql_params['db'], cursorclass=MySQLdb.cursors.DictCursor)
    except MySQLdb.Error, e:
        logger.error(e)
        raise
    return connection

def process_mpesa_request(db, conn):
    global is_processing, is_waiting_to_die
    # Get transactions
    req_list = db.get_pending_transactions(100)

    if not req_list:
        return
    try:
        is_processing = True

        for req in req_list:
            db._delete_transaction(req['id'])

            msisdn = req['msisdn']
            name = req['name']
            amount = req['amount']
            smsc_id = req['SMSCID']
	    tran_id = req['tran_id'] 
            response = engine.deposit(db, msisdn, name, amount, tran_id)

            db.queue_sms(defaults['originator'], msisdn, response, smsc_id)
            db._push_to_processed(req)
        conn.commit()

        is_processing = False

        if is_waiting_to_die:
            logger.info('NOT PROCESSING:\tDIE.')
            exit(0)
    except Exception, e:
        logger.error(e)
        conn.rollback()
        is_processing = 0
        if is_waiting_to_die:
            logger.info('NOT PROCESSING:\tDIE.')
            exit(0)

def process_incoming_request(db, conn):
    global is_processing, is_waiting_to_die
    # Get transactions
    req_list = db.get_pending_requests(100)

    if not req_list:
        return
    try:
        is_processing = True

        for req in req_list:
            #RecordID, Originator, Destination, Message, MessageTimeStamp, MessageDirection, SMSCID
            db._delete_record(req['RecordID'])
            msisdn = req['Originator']
            destination = req['Destination']
            message = req['Message'].lower()
            smsc_id = req['SMSCID']
            response = None
            logger.info('Processing [MSISDN] %s [Destination] %s [Message] %s [SMSCID] %s' 
                %(req['Originator'],  req['Destination'], req['Message'], req['SMSCID']))

            if re.match(r'^(' + keyword['register'] + ").*", message):
                response = engine.register_user(db, msisdn, message, 0)
            elif re.match(r'^(' + keyword['balance'] + ").*", message):
                response = engine.check_balance(db, msisdn, message)
            elif re.match(r'^(' + keyword['bet'] + ").*", message):    
                response = engine.issue_match_ticket(db, msisdn, message.upper())
            elif re.match(r'^(' + keyword['withdraw'] + ").*", message):    
                response = engine.withdraw(db, msisdn, message)
            else:
                response = sms_templates['default_sms']

            db.queue_sms(destination, msisdn, response, smsc_id)

            db._push_to_cdr(req)
        conn.commit()

        is_processing = False

        if is_waiting_to_die:
            logger.info('NOT PROCESSING:\tDIE.')
            exit(0)
    except Exception, e:
        logger.error(e)
        conn.rollback()
        is_processing = 0
        if is_waiting_to_die:
            logger.info('NOT PROCESSING:\tDIE.')
            exit(0)

if __name__=='__main__':
    usage()
    signal.signal(signal.SIGTERM, signal_handler)
    logger.info('%s: Started! Name: %s %s'
	 % (datetime.datetime.now(), sys.argv[1], sibsco['name']))

    engine = Engine()
    while True:
        try:
            conn = create_connection()
            conn.autocommit(False) # Disable auto commit
            db = Database(conn);
            if sys.argv[1] == "sms":
                process_incoming_request(db, conn)
            else:
                process_mpesa_request(db, conn)
            
        except MySQLdb.Error, e:
            conn.commit()
            logger.error("MySQL Error: %s" % e)
        finally:
            try:
                conn.close()
            except Exception ,e:
                logger.error("MySQL Error closing connection : %s" % e)
        time.sleep(1)
