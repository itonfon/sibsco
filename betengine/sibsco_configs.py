import logging                                                                                 
import ConfigParser                                                                            
CONFIG_FILE = r'/usr/local/lib/betengine/sibsco.conf'
config = ConfigParser.ConfigParser()                                                           
config.read(CONFIG_FILE)                                                                       
#logging.basicConfig(level=logging.INFO)                                                        
logger = logging.getLogger("LOTTO TICKETING")                                                              
logger.setLevel(logging.DEBUG)
#logger.setLevel(logging.INFO)
#hdlr = logging.FileHandler(config.get("logger", "log_file"))                                   
hdlr = logging.StreamHandler()                                   
formatter = logging.Formatter('%(asctime)s %(name)s %(levelname)s %(message)s')                         
hdlr.setFormatter(formatter)                                                                   
logger.addHandler(hdlr)                                                                        
                                                                                                                                                           
host = config.get("mysql", "host")                                                             
port = config.get("mysql", "port")                                                             
user = config.get("mysql", "user")                                                             
passwd = config.get("mysql", "password")                                                       
db = config.get("mysql", "database")                                                           
connection_timeout = config.get("mysql", "connection_timeout")                                 
mysql_params = {
        'host':host,
        'port':port,
        'user':user,
        'passwd':passwd,
        'db':db,
        'connection_timeout':connection_timeout        
        }
       
welcome_sms = config.get("sms-template", "welcome_sms") 
user_exists = config.get("sms-template", "user_exists")
bal_resp = config.get("sms-template", "bal_resp")
not_registered = config.get("sms-template", "not_registered")
deposit_success = config.get("sms-template", "deposit_success")
low_withd_bal = config.get("sms-template", "low_withd_bal")
withd_success = config.get("sms-template", "withd_success")
invalid_command = config.get("sms-template", "invalid_command")
ticket_sms = config.get("sms-template", "ticket_sms")
low_balance = config.get("sms-template", "low_balance")
default_sms = config.get("sms-template", "default_sms")
ticket_limit_exceed = config.get("sms-template", "ticket_limit_exceed")   




sms_templates = {                                                                          
    'welcome_sms':welcome_sms,
    'user_exists':user_exists, 
    'bal_resp':bal_resp,  
    'not_registered':not_registered,  
    'deposit_success':deposit_success,  
    'low_withd_bal':low_withd_bal,  
    'withd_success':withd_success,                                                                 
    'invalid_command':invalid_command,  
    'ticket_sms':ticket_sms,  
    'low_balance':low_balance,  
    'default_sms':default_sms,  
    'ticket_limit_exceed':ticket_limit_exceed,  
}

withdraw_max = config.get("defaults", "withdraw_max")                                                                                                                    
withdraw_min = config.get("defaults", "withdraw_min")
bet_amount  = config.get("defaults", "bet_amount") 
draw_date = config.get("defaults", "draw_date")                                                                                                                    
draw_time = config.get("defaults", "draw_time")
originator  = config.get("defaults", "originator") 
bet_limit = config.get("defaults", "bet_limit")                                                                                                                    
ticket_length = config.get("defaults", "ticket_length")
valid_prediction  = config.get("defaults", "valid_prediction") 

defaults = {
    'withdraw_max':withdraw_max,
    'withdraw_min':withdraw_min,
    'bet_amount' : bet_amount, 
    'draw_date':draw_date,
    'draw_time':draw_time,
    'originator' : originator,
    'bet_limit':bet_limit,
    'ticket_length':ticket_length,
    'valid_prediction' : valid_prediction,  
}

register = config.get("keyword", "register")                                                                                                                    
balance = config.get("keyword", "balance")
bet  = config.get("keyword", "bet")
withdraw  = config.get("keyword", "withdraw")

keyword = {
    'register':register,
    'balance':balance,
    'bet' : bet,
    'withdraw' : withdraw, 
}

name  = config.get("sibsco", "name")
sibsco = {
    'name':name
}
